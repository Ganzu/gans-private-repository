<?php
/**
 * @package SKT IT Consultant
 */
?>
<div class="content-area">
	<div class="customContainer">
		<div id="secondNav">
			<img src="<?php bloginfo('stylesheet_directory');?>/images/video_icon.png" width="20px" height="20px"/>
			VIDEO GALLERY
		</div>

		<!-- Now Playing Video -->
			<!-- Slider Part -->
			<div id="videoGalleryContainer">				
				<!-- Left Button -->			
				<div id="video-slider-left-button">
		        	<div class="arrowButton left" style="border-color: #062340"></div>
		      	</div>
				<!-- Videos for you section, suggested videos for user -->
				<div id="videoSliderContainer">
					<div class="sv-slider">
						<div id="mandatoryDiv2">							
							<?php 
								$args = array( 'posts_per_page' => -1,'category' => 12, 'orderby' => 'rand' );					
								$videos = get_posts($args);	
								$vidCount = 0;

								foreach($videos as $video)
								{ 
									$originalContent = $video->post_content;
									//echo $originalContent;
									$content = preg_split('/[\s]+/',$video->post_content);
									//remove the tags from the start and end of the link
									$link = substr(substr($content[0],7),0,-8); 
									//$link = substr($content,0,-8);
									$description = str_replace($content[0],'',$originalContent); 
								?>
								
								<div class="slidesContainer">
									<div id="videoPlayingContainer">
										<div id="videoPlaying">		
											<!-- Video title -->
											<div>
												<h1 id="videoTitle">Videos:  <?php echo $video->post_title; ?></h1>
											</div>
											<!-- Individual video container -->									
											<div id="video">												
												<!-- Video -->
												<iframe width="70%" height="434px" src="<?php echo $link?>" frameborder="0" allowfullscreen></iframe>
											</div>
											<div id="videoTextContainer">
												<!--div class="testdiv">
													<!-- Video caption -->
													<div class="vidCaption" id="videoCaption">
													"Getting all the information you need before you decide on a pathway is very important"</div>
													<!-- Video description -->
													<div class="videoDescription" id="videoDesc"><?php echo $description;?></div>
												<!--/div-->
											</div>											
										</div>
									</div>
								</div>
								
								<?php } ?>
						</div>
					</div>
				</div>
				<!-- Right button -->
				<div id="video-slider-right-button">
		        	<div class="arrowButton right" style="border-color: #062340"></div>
		      	</div>
		    </div>

		<!-- *********** Videos For You *********** -->
			<div id="suggestedVideoContainer">
			<!-- Title: Videos For You -->
			<h1 id="videosForYou">Videos For You</h1>

			<!-- Slider Part -->
			<div id="videoGalleryContainer">
				<!-- Left Button -->			
				<div id="vpLeftButton">
		        	<div class="arrowButton left"></div>
		      	</div>
				<!-- Videos for you section, suggested videos for user -->
				<div id="vSliderContainer">
					<div class="v-slider">
						<div id="mandatoryDiv">
							<!-- Each slideContainer contains 4 videos -->
							<div class="slideContainer">
								<div class="videoSlide">
									<?php $counter = 0;
											query_posts('cat=12&orderby=rand');
											if(have_posts()) : 
												while(have_posts()) : the_post();
													if((($counter%4)==0) && ($counter!=0)) :
									?>
								</div>
							</div>	
							<div class="slideContainer">
								<div class="videoSlide">							
													
									<?php			endif; ?>
									
									<div class="videoContainer">
										<?php 
											$originalContent = get_the_content();
											//echo $originalContent;
											$content = preg_split('/[\s]+/',get_the_content());
											//remove the tags from the start and end of the link
											$link = substr(substr($content[0],7),0,-8); 
											//$link = substr($content,0,-8);
											$description = str_replace($content[0],'',$originalContent);
										?>
											<iframe width="100%" height="55%" src="<?php echo $link?>" frameborder="0" allowfullscreen></iframe>
											<!-- Video title -->
											<div class="videoTextContainerMini">
												<h1 class="videoTitleMini"><?php the_title(''); ?></h1>
												<?php echo $description; $counter++;?>
											</div>											
									</div>									
									<?php
												endwhile;									
											endif;
											if($counter%4!=0 ) : 
												echo ($counter);
											endif;	
											wp_reset_query();												
									?>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<!-- Right button -->
				<div id="vpRightButton">
		        	<div class="arrowButton right"></div>
		      	</div>
		    </div>

		    <!-- View all button -->
		    <div id="viewAllVideoButtonContainer">
		    	<button type="button" class="btn-01">View All</button>
		    </div>
	    </div>
		
		
		<div id="articles-highlight">
			<h2>Related Articles</h2>			
			<div class="articleContainer">	        				
				<div class="articlePartition"> 
					
						<?php
							$count = 0;
							query_posts('cat=(2,3,6,8,-12)&orderby=rand');
							if(have_posts()) : 
								while((have_posts()) && ($count < 2)) : the_post();
								
						?> 		<div class="articles">				
						<div class="articleContent">									
							<?php
								$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
								$image = aq_resize( $url, 200, 188, true );										
								if(!empty($url)){
									echo '<img src="'. $image. '" longdesc="URL_2" alt="Text_2" />';
								}
							?>										
							<h1><?php the_title(''); ?></h1><p></p>
							<?php the_content($more_link_text , $strip_teaser); $count++?> 
						</div>					
					</div>
						<?php
							endwhile;
							endif;
							wp_reset_query();
						?>		
							
				</div>
			</div>
			<div id="viewAllVideoButtonContainer">
		    	<button type="button" class="btn-01">View All</button>
		    </div>
		</div>
	</div>
</div>
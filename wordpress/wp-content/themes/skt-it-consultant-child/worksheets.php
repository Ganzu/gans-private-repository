<?php
	/*
	Template Name: Worksheet Page
	*/
	get_header(); 
?>

	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'worksheets_form', 'worksheets' ); ?>
	<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
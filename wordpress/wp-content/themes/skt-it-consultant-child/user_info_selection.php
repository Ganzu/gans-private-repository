<!-- This php page is used from the index.php/ landing page of the site.
	 I put it seperately just to make it easier to read and modify as it contains different
	 functionalities.
-->

<!-- User info -->
<div id="userinfo">
	<div id="userinfo-container">
		<div id="userinfo-title">
			<h2>About the person diagnosed</h2>
			<a href="#" class="moreInfo">?</a>	
			<!-- Pop up tooltip box -->
			<div class="toolTip">
				<div class="toolTipText">We use this information to tailor the site to suit your needs.</div>
			</div>
			<div id="userinfo-selection">
				<div id="userinfo-selection-table">
					<select class="userinfo-select-item">
						<option value="" disabled selected>Age</option>
						<option value="">40-45</option>
						<option value="">45-50</option>
						<option value="">50-55</option>
						<option value="">55-60</option>
						<option value="">60-65</option>
						<option value="">65-70</option>
						<option value="">70+</option>
					</select>
					<select class="userinfo-select-item">
						<option value="" disabled selected>Diagnosis</option>
						<option value="">Option 1</option>
						<option value="">Option 2</option>
						<option value="">Option 3</option>
					</select>
					<select class="userinfo-select-item last">
						<option value="" disabled selected>Sexuality</option>
						<option value="">Gay</option>
						<option value="">Straight</option>
					</select>
				</div>				
			</div>
		</div>

	</div>
</div>

<!-- Articles for you -->
<div id="articles-highlight">
	<h2>Articles for you</h2>
	<div id="sliderContainer">
		<!-- Load jquery file article-slider.js -->
      	<div id="slider-left-button">
        	<div class="arrowButton left"></div>
      	</div>
      	<div id="test">							
			<div class="sa-slider"><!--The slider-->
				<div id="mandatoryDiv"><!-- A mandatory div used by the slider -->
	        		<!-- Each div below is considered a slide -->
	        		<div class="sliderDiv">
	        			<div class="articleContainer">	        				
							<div class="articlePartition"> 
									<?php
										//global $more;
										//$more = 0;
										$count = 0;
										query_posts('cat=(2,3,6,8)&orderby=rand');
										if(have_posts()) : 
											while((have_posts()) && ($count < 8)) : the_post();
											if((($count%2)==0) && ($count!=0)) : 
									?> 
							</div>
									<?php 
										if(($count==4)) : 
									?>
						</div>
					</div>

					<!-- Each div below is considered a slide -->
					<div class="sliderDiv">
						<div class="articleContainer">
							<?php 
								endif;
							?>
							<div class="articlePartition"> 
								<?php 
									endif;
								?>
								<div class="articles">
									<div class="articleContent">									
										<?php
											$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
											$image = aq_resize( $url, 200, 188, true );										
											if(!empty($url)){
												echo '<img src="'. $image. '" longdesc="URL_2" alt="Text_2" />';
											}
										?>										
										<!-- <img src="htt[://www.google.com"></img> -->
										<div class="articleTextContainer">
										<td>
											<h1><?php the_title(''); ?></h1><p></p>
											<?php the_content($more_link_text , $strip_teaser); $count++;?> 											
										</td>
										</div>
										<a href="#">Read More</a>
									</div>
								</div>
								<?php
									endwhile;
									endif;
									wp_reset_query();
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="slider-right-button">
			<div class="arrowButton right"></div>
		</div>		
	</div>
</div>


<!-- Support for you -->
<div id="support-highlight">
	<h2>Support & Resources</h2>
	<div id="supportContainer">
		<!-- Load jquery file article-slider.js -->
     
      	<div id="test1">							

	        		<div class="supportDiv">
	        			<div class="supportContainer">	        				
							<div class="supportPartition"> 								
								<div class="support">
									<div class="supportContent">
									<img class="support-img" src=<?php bloginfo('stylesheet_directory');?>/images/chat.png />
									<h4>Chat rooms & Forums</h4>
									<h5>Lorem ipsum dolor sit amet, consectetur <br />adipiscing elit. Aliquam at porttitor sem. <br />Aliquam erat volutpat.<br /></h5>
									<button type="button" class="btn-02" onclick="window.location.href='http://localhost/wordpress/?page_id=83'">View</button>
									</div>
								</div>
								<div class="support">
									<div class="supportContent">
									<img class="support-img" src=<?php bloginfo('stylesheet_directory');?>/images/health.png />
									<h4>Health & Lifestyle</h4>
									<h5>Lorem ipsum dolor sit amet, consectetur <br />adipiscing elit. Aliquam at porttitor sem. <br />Aliquam erat volutpat.<br /></h5>
									<button type="button" class="btn-02" onclick="window.location.href='http://localhost/wordpress/?page_id=86'">View</button>
									</div>
								</div>
								<div class="support">
									<div class="supportContent">
									<img class="support-img" src=<?php bloginfo('stylesheet_directory');?>/images/support.png />
									<h4>Support Groups</h4>
									<h5>Lorem ipsum dolor sit amet, consectetur <br />adipiscing elit. Aliquam at porttitor sem. <br />Aliquam erat volutpat.<br /></h5>
									<button type="button" class="btn-02" onclick="window.location.href='http://localhost/wordpress/?page_id=88'">View</button>
									</div>
								</div>
							</div>
							<div class="supportPartition">
								<div class="support-mobile">
									<a href="http://localhost/wordpress/?page_id=83">
									<div class="supportContent-mobile">
										<img class="support-img" src=<?php bloginfo('stylesheet_directory');?>/images/chat.png />
										<h4>Chat rooms & Forums</h4>
										</a>
									</div>
								</div>
								<div class="support-mobile">
									<a href="http://localhost/wordpress/?page_id=86">
									<div class="supportContent-mobile">
										<img class="support-img" src=<?php bloginfo('stylesheet_directory');?>/images/health.png />
										<h4>Health & Lifestyle</h4>
										</a>
									</div>
								</div>
							</div>
							<div class="supportPartition">
								<div class="support-mobile">
									<a href="http://localhost/wordpress/?page_id=88">
									<div class="supportContent-mobile">
										<img class="support-img" src=<?php bloginfo('stylesheet_directory');?>/images/support.png />
										<h4>Support Groups</h4>
										</a>
									</div>
								</div>
								
							</div>
						</div>
					</div>
		</div>
	</div>
</div>
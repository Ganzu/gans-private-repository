<?php /* Template Name: Embed Articles */ 
/**
 * The template for displaying home page.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package SKT IT Consultant
 */

get_header(); 
?>

<!-- Load user user_info_selection.php file -->
<?php get_template_part( 'user_info_selection' ); ?>    

<?php if ( 'page' == get_option( 'show_on_front' ) && ( '' != get_option( 'page_for_posts' ) ) && $wp_query->get_queried_object_id() == get_option( 'page_for_posts' ) ) : ?>

    <div class="content-area">
        <div class="container">
            <section class="site-main" id="sitemain">
                <div class="blog-post">
					<?php
                    if ( have_posts() ) :
                        // Start the Loop.
                        while ( have_posts() ) : the_post();
                            /*
                             * Include the post format-specific template for the content. If you want to
                             * use this in a child theme, then include a file called called content-___.php
                             * (where ___ is the post format) and that will be used instead.
                             */
                            get_template_part( 'content', get_post_format() );
                    
                        endwhile;
                        // Previous/next post navigation.
                        skt_itconsultant_pagination();
                    
                    else :
                        // If no content, include the "No posts found" template.
                         get_template_part( 'no-results', 'index' );
                    
                    endif;
                    ?>		


					<br />
					<?php
						//global $more;
						//$more = 0;
						query_posts('cat=2');
						if(have_posts()) : 
							while(have_posts()) : the_post();
					?></p>
					<p><?php the_title(''); ?> </p>
					<p><?php the_content(''); ?> </p>
					<p>
					<?php
						endwhile;
						endif;
						wp_reset_query();
					?><br/></p>
                </div><!-- blog-post -->
            </section>
            <div class="left">
            <?php get_sidebar('left');?>
            </div>
            <div class="clear"></div>
        </div>
        
    </div>
<img src="<?php echo get_stylesheet_directory_uri()."/images/dthumb.jpg"; ?>">
<?php else: ?>
    <!-- DELETED EVERYTHING IN THESE TAGS
<?php endif; ?>


<?php get_footer(); ?>
 <?php
	/*
	Template Name: Sub Resource & Forums Page
	*/
	get_header(); 
?>

	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'custom-support-content', 'custom-support' ); ?>
	<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
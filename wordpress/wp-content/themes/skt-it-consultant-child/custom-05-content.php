<?php
/**
 * @package SKT IT Consultant
 * This page is a template for the Support & Resources Page
 */
?>
		<!-- Support for you -->
<div id="supportresource-highlight">
	<div id="supportContainer">
	<div id="secondNav">
			<img src="<?php bloginfo('stylesheet_directory');?>/images/video_icon.png" width="20px" height="20px"/>
			VIDEO GALLERY
	</div>
		<h2>Support & Resources</h2>
		<!-- Load jquery file article-slider.js -->
    	<div id="test1">							

	        		<div class="supportDiv">
	        			<div class="supportContainer">	        				
							<div class="supportPartition"> 								
								<div class="support">
									<div class="supportContent">
									<img class="support-img" src=<?php bloginfo('stylesheet_directory');?>/images/chat.png />
									<h4>Chat rooms & Forums</h4>
									<h5>Lorem ipsum dolor sit amet, consectetur <br />adipiscing elit. Aliquam at porttitor sem. <br />Aliquam erat volutpat.<br /></h5>
										<button type="button" class="btn-02" onclick="window.location.href='http://52.64.62.176/wordpress/?page_id=260'">View</button>
									</div>
								</div>
								<div class="support">
									<div class="supportContent">
									<img class="support-img" src=<?php bloginfo('stylesheet_directory');?>/images/health.png />
									<h4>Health & Lifestyle</h4>
									<h5>Lorem ipsum dolor sit amet, consectetur <br />adipiscing elit. Aliquam at porttitor sem. <br />Aliquam erat volutpat.<br /></h5>
									<button type="button" class="btn-02" onclick="window.location.href='http://52.64.62.176/wordpress/?page_id=263'">View</button>
									</div>
								</div>
								<div class="support">
									<div class="supportContent">
									<img class="support-img" src=<?php bloginfo('stylesheet_directory');?>/images/support.png />
									<h4>Support Groups</h4>
									<h5>Lorem ipsum dolor sit amet, consectetur <br />adipiscing elit. Aliquam at porttitor sem. <br />Aliquam erat volutpat.<br /></h5>
								<button type="button" class="btn-02" onclick="window.location.href='http://52.64.62.176/wordpress/?page_id=265'">View</button>
									</div>
								</div>
								
							</div>
							<div class="supportPartition"> 	
								<div class="support">
									<div class="supportContent">
									<img class="support-img" src=<?php bloginfo('stylesheet_directory');?>/images/theraphy.png />
									<h4>Complementary Therapies</h4>
									<h5>Lorem ipsum dolor sit amet, consectetur <br />adipiscing elit. Aliquam at porttitor sem. <br />Aliquam erat volutpat.<br /></h5>
										<button type="button" class="btn-02" onclick="window.location.href='http://52.64.62.176/wordpress/?page_id=270'">View</button>
									</div>
								</div>
								<div class="support">
									<div class="supportContent">
									<img class="support-img" src=<?php bloginfo('stylesheet_directory');?>/images/partner.png />
									<h4>Partners Support</h4>
									<h5>Lorem ipsum dolor sit amet, consectetur <br />adipiscing elit. Aliquam at porttitor sem. <br />Aliquam erat volutpat.<br /></h5>
										<button type="button" class="btn-02" onclick="window.location.href='http://52.64.62.176/wordpress/?/?page_id=274'">View</button>
									</div>
								</div>
							</div>
								<div class="supportPartition">
								<div class="support-mobile">
									<a href="'http://52.64.62.176/wordpress/?page_id=263'">
									<div class="supportContent-mobile">
										<img class="support-img" src=<?php bloginfo('stylesheet_directory');?>/images/chat.png />
										<h4>Chat rooms & Forums</h4>
										</a>
									</div>
								</div>
								<div class="support-mobile">
									<a href="'http://52.64.62.176/wordpress/?page_id=260'">
									<div class="supportContent-mobile">
										<img class="support-img" src=<?php bloginfo('stylesheet_directory');?>/images/health.png />
										<h4>Health & Lifestyle</h4>
										</a>
									</div>
								</div>
							</div>
							<div class="supportPartition">
								<div class="support-mobile">
									<a href="'http://52.64.62.176/wordpress/?page_id=265'">
									<div class="supportContent-mobile">
										<img class="support-img" src=<?php bloginfo('stylesheet_directory');?>/images/support.png />
										<h4>Support Groups</h4>
										</a>
									</div>
								</div>
								<div class="support-mobile">
									<a href="'http://52.64.62.176/wordpress/?page_id=270'">
									<div class="supportContent-mobile">
										<img class="support-img" src=<?php bloginfo('stylesheet_directory');?>/images/chat.png />
										<h4>Complementary Therapies</h4>
										</a>
									</div>
								</div>
							</div>
							<div class="supportPartition">
								<div class="support-mobile">
									<a href="'http://52.64.62.176/wordpress/?page_id=274'">
									<div class="supportContent-mobile">
										<img class="support-img" src=<?php bloginfo('stylesheet_directory');?>/images/health.png  />
										<h4>Partners Support</h4>
										</a>
									</div>
								</div>
							
							</div>
							
						</div>
					</div>
		</div>
	</div>
</div>
 <?php
	/*
	Template Name: Generic Video Gallery Custom Page
	*/
	get_header(); 
?>

	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'video_gallery_generic', 'video_gallery_generic_template' ); ?>
	<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
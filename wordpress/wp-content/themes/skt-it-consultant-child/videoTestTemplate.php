 <?php
	/*
	Template Name: Video Gallery Test Template Page
	*/
	get_header(); 
?>

	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'videoTest', 'videoTestTemplate' ); ?>
	<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>

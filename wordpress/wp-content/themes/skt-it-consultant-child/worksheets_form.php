<?php
/**
 * @package SKT IT Consultant
 */
?>
<div class="content-area">
	<div class="customContainer">
		<div id="secondNav">
			<img src="<?php bloginfo('stylesheet_directory');?>/images/video_icon.png" width="20px" height="20px"/>
			VIDEO GALLERY
		</div>
		<!--Outer Container-->
		<div id="infoContainer">
			<!-- Left side navigation of info items -->
            <div id="leftInfoNavigation">
				<!-- Entire set for one item in the info nav -->
				<div class="leftInfoHeading">
					<h2><?php echo get_cat_name(2); ?></h2>		
					<div class="expandableButton"></div>			
				</div>
				<div class="leftInfoContent">
					<?php query_posts('cat=2'); ?>
                    <?php while (have_posts()) : the_post(); ?>
					<p class="content_<?php the_ID();?> content_title_navi articleTitleLeft"><?php the_title(); ?></p>
                    <?php endwhile; wp_reset_query(); ?>
				</div>
				<!-- Info nav set ends here -->
				<!-- Entire set for one item in the info nav -->
				<div class="leftInfoHeading">
					<h2>Header-1</h2>		
					<div class="expandableButton"></div>			
				</div>
				<div class="leftInfoContent">
					<p>Lorem ipsum stuff</p>
					<p>More lorem ipsum stuff</p>
				</div>
				<!-- Info nav set ends here -->
				<!-- Entire set for one item in the info nav -->
				<div class="leftInfoHeading">
					<h2>Header-1</h2>		
					<div class="expandableButton"></div>			
				</div>
				<div class="leftInfoContent">
					<p>Lorem ipsum stuff</p>
					<p>More lorem ipsum stuff</p>
				</div>
				<!-- Info nav set ends here -->
			</div>			
			
			<!--Right side Container-->
            <div class="rightContentInfo">
            	<div class="rightArticleContainer" style="text-align:center;">
                    <div id="worksheet_1">
                    <?php if(isset($_POST['_ninja_forms_display_submit'])){echo 'Your form has been successfully submitted.';}?>
					<div><h1>Men's Worksheet - Active Surveillance</h1></div>
					<p style = "line-height: 140%; font-size: 15px;">
						<font color="#615e5e">
							Some pros and cons are listed in the table below, with a space at the bottom where you can write your thoughts if you wish.
						</font>
					</p>
					<form name="ninja_forms_form_7" action="" method="post">
                                            <input type="hidden" id="_wpnonce" name="_wpnonce" value="52826767bf">
                                            <input type="hidden" name="_ninja_forms_display_submit" value="1">
                                            <input type="hidden" name="_form_id" id="_form_id" value="7">
						<div id="bloke_worksheet" class="table form" style="display:table; border-top:0px solid black;">	
							<div class="table-row">
								<div class="table-head" style="width: 10%"></div>
								<div class="table-head" style="width: 40%">Points to consider</div>
								<div class="table-head" style="width: 25%">Personal response</div>
								<div class="table-head" style="width: 25%">Level of importance</div>
							</div>
							
							<div class="table-row">
								<div class="table-cell"> 
								</div>
								<div class="table-cell">
										I will maintain my sexual function and be free from incontinence or complications as a result of surgery or radiotherapy
								</div>
								<div class="table-cell">
                                                                    <textarea name="ninja_forms_field_16" id="ninja_forms_field_16" class="ninja-forms-field " rel="16" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" style="border:0;resize: none;"></textarea>
								</div>
								<div class="table-cell">	 
									<input id="ninja_forms_field_9_0" name="ninja_forms_field_9" type="radio" class="ninja-forms-field " value="Very important" rel="9"> Very important<br />
									<input id="ninja_forms_field_9_1" name="ninja_forms_field_9" type="radio" class="ninja-forms-field " value="Somewhat important" rel="9"> Somewhat important<br />
									<input id="ninja_forms_field_9_2" name="ninja_forms_field_9" type="radio" class="ninja-forms-field " value="Not important" rel="9"> Not important<br /> 
								</div>								
							</div>
							
							<div class="table-row">
								<div class="table-cell"> 
								</div>
								<div class="table-cell">
										I can delay treatment side effects or may never need another treatment
								</div>
								<div class="table-cell">
                                                                    <textarea name="ninja_forms_field_17" id="ninja_forms_field_17" class="ninja-forms-field " rel="17" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" style="border:0;resize: none;"></textarea>
								</div>
								<div class="table-cell">	 
									<input id="ninja_forms_field_11_0" name="ninja_forms_field_11" type="radio" class="ninja-forms-field" value="Very important" rel="11"> Very important<br />
									<input id="ninja_forms_field_11_1" name="ninja_forms_field_11" type="radio" class="ninja-forms-field" value="Somewhat important" rel="11"> Somewhat important<br />
									<input id="ninja_forms_field_11_2" name="ninja_forms_field_11" type="radio" class="ninja-forms-field" value="Not important" rel="11"> Not important<br /> 
								</div>									
							</div>
							
							
							<div class="table-row">
								<div class="table-cell"> 
								</div>
								<div class="table-cell">
										I am being closely monitored
								</div>
								<div class="table-cell">
                                                                    <textarea name="ninja_forms_field_18" id="ninja_forms_field_18" class="ninja-forms-field " rel="18" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" style="border:0;resize: none;"></textarea>
								</div>
								<div class="table-cell">	 
									<input id="ninja_forms_field_12_0" name="ninja_forms_field_12" type="radio" class="ninja-forms-field " value="Very important" rel="12">Very important<br />
									<input id="ninja_forms_field_12_1" name="ninja_forms_field_12" type="radio" class="ninja-forms-field " value="Somewhat important" rel="12"> Somewhat important<br />
									<input id="ninja_forms_field_12_2" name="ninja_forms_field_12" type="radio" class="ninja-forms-field " value="Not important" rel="12"> Not important<br /> 
								</div>								
							</div>
							
							
							<div class="table-row">
								<div class="table-cell"> 
								</div>
								<div class="table-cell">
										I can live relatively normally
								</div>
								<div class="table-cell">
                                                                    <textarea name="ninja_forms_field_19" id="ninja_forms_field_19" class="ninja-forms-field " rel="19" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" style="border:0;resize: none;"></textarea>
								</div>
								<div class="table-cell">	 
									<input id="ninja_forms_field_13_0" name="ninja_forms_field_13" type="radio" class="ninja-forms-field " value="Very important" rel="13"> Very important<br />
									<input id="ninja_forms_field_13_1" name="ninja_forms_field_13" type="radio" class="ninja-forms-field " value="Somewhat important" rel="13"> Somewhat important<br />
									<input id="ninja_forms_field_13_2" name="ninja_forms_field_13" type="radio" class="ninja-forms-field " value="Not important" rel="13"> Not important<br /> 
								</div>								
							</div>
							
							
							<div class="table-row">
								<div class="table-cell"> 
								</div>
								<div class="table-cell">
										Other pros:
								</div>
								<div class="table-cell">
                                                                    <textarea name="ninja_forms_field_20" id="ninja_forms_field_20" class="ninja-forms-field " rel="20" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" style="border:0;resize: none;"></textarea>
								</div>
								<div class="table-cell">	 
									<input id="ninja_forms_field_14_0" name="ninja_forms_field_14" type="radio" class="ninja-forms-field " value="Very important" rel="14"> Very important<br />
									<input id="ninja_forms_field_14_1" name="ninja_forms_field_14" type="radio" class="ninja-forms-field " value="Somewhat important" rel="14"> Somewhat important<br />
									<input id="ninja_forms_field_14_2" name="ninja_forms_field_14" type="radio" class="ninja-forms-field " value="Not important" rel="14"> Not important<br /> 
								</div>			
							</div>
							
							<div class="line">
								 <svg version="1.2" width="100%" height="100%">
								  <line x1="0" y1="5.5" x2="829" y2="5.5" stroke="#615e5e" stroke-width="2" stroke-dasharray="" stroke-linecap="square"></line>
								 </svg>
							</div>
							
							<div class="table-row">
								<div class="table-cell"> 
								</div>
								<div class="table-cell">
										In the unlikely event of the prostate cancer spreading it may not be curable
								</div>
								<div class="table-cell">
                                                                    <textarea name="ninja_forms_field_22" id="ninja_forms_field_22" class="ninja-forms-field " rel="22" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" style="border:0;resize: none;"></textarea>
								</div>
								<div class="table-cell">	 
									<input id="ninja_forms_field_21_0" name="ninja_forms_field_21" type="radio" class="ninja-forms-field " value="Very important" rel="21"> Very important<br />
									<input id="ninja_forms_field_21_1" name="ninja_forms_field_21" type="radio" class="ninja-forms-field " value="Somewhat important" rel="21"> Somewhat important<br />
									<input id="ninja_forms_field_21_2" name="ninja_forms_field_21" type="radio" class="ninja-forms-field " value="Not important" rel="21"> Not important<br /> 
								</div>									
							</div>
							
							<div class="table-row">
								<div class="table-cell"> 
								</div>
								<div class="table-cell">
										The prostate cancer may grow and cause problems
								</div>
								<div class="table-cell">
                                                                    <textarea name="ninja_forms_field_24" id="ninja_forms_field_24" class="ninja-forms-field " rel="24" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" style="border:0;resize: none;"></textarea>
								</div>
								<div class="table-cell">	 
									<input id="ninja_forms_field_23_0" name="ninja_forms_field_23" type="radio" class="ninja-forms-field " value="Very important" rel="23"> Very important<br />
									<input id="ninja_forms_field_23_1" name="ninja_forms_field_23" type="radio" class="ninja-forms-field " value="Somewhat important" rel="23"> Somewhat important<br />
									<input id="ninja_forms_field_23_2" name="ninja_forms_field_23" type="radio" class="ninja-forms-field " value="Not important" rel="23"> Not important<br /> 
								</div>								
							</div>
							
							<div class="table-row">
								<div class="table-cell"> 
								</div>
								<div class="table-cell">
										There may be side effects from biopsies and inconvenience with regular testing
								</div>
								<div class="table-cell">
                                                                    <textarea name="ninja_forms_field_26" id="ninja_forms_field_26" class="ninja-forms-field " rel="26" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" style="border:0;resize: none;"></textarea>
								</div>
								<div class="table-cell">	 
									<input id="ninja_forms_field_25_0" name="ninja_forms_field_25" type="radio" class="ninja-forms-field " value="Very important" rel="25"> Very important<br />
									<input id="ninja_forms_field_25_1" name="ninja_forms_field_25" type="radio" class="ninja-forms-field " value="Somewhat important" rel="25"> Somewhat important<br />
									<input id="ninja_forms_field_25_2" name="ninja_forms_field_25" type="radio" class="ninja-forms-field " value="Not important" rel="25"> Not important<br /> 
								</div>								
							</div>
							
							<div class="table-row">
								<div class="table-cell"> 
								</div>
								<div class="table-cell">
										It can be hard to live with uncertainty
								</div>
								<div class="table-cell">
                                                                    <textarea name="ninja_forms_field_28" id="ninja_forms_field_28" class="ninja-forms-field " rel="28" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" style="border:0;resize: none;"></textarea>
								</div>
								<div class="table-cell">	 
									<input id="ninja_forms_field_27_0" name="ninja_forms_field_27" type="radio" class="ninja-forms-field " value="Very important" rel="27"> Very important<br />
									<input id="ninja_forms_field_27_1" name="ninja_forms_field_27" type="radio" class="ninja-forms-field " value="Somewhat important" rel="27"> Somewhat important<br />
									<input id="ninja_forms_field_27_2" name="ninja_forms_field_27" type="radio" class="ninja-forms-field " value="Not important" rel="27"> Not important<br /> 
								</div>									
							</div>
							
							<div class="table-row">
								<div class="table-cell"> 
								</div>
								<div class="table-cell">
										Other cons:
								</div>
								<div class="table-cell">
                                                                    <textarea name="ninja_forms_field_30" id="ninja_forms_field_30" class="ninja-forms-field " rel="30" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" style="border:0;resize: none;"></textarea>
								</div>
								<div class="table-cell">	 
									<input id="ninja_forms_field_29_0" name="ninja_forms_field_29" type="radio" class="ninja-forms-field " value="Very important" rel="29"> Very important<br />
									<input id="ninja_forms_field_29_1" name="ninja_forms_field_29" type="radio" class="ninja-forms-field " value="Somewhat important" rel="29"> Somewhat important<br />
									<input id="ninja_forms_field_29_2" name="ninja_forms_field_29" type="radio" class="ninja-forms-field " value="Not important" rel="29"> Not important<br /> 
								</div>									
							</div>							
						</div><br /><br /> <br />
						<p class="para">
							<strong>Overall, are the pros or cons more important to me?</strong> &nbsp;&nbsp;
							<input id="ninja_forms_field_31_0" name="ninja_forms_field_31" type="radio" class="ninja-forms-field " value="Pros" rel="31">Pros &nbsp;&nbsp;
							<input id="ninja_forms_field_31_1" name="ninja_forms_field_31" type="radio" class="ninja-forms-field " value="Cons" rel="31">Cons &nbsp;&nbsp;
							<input id="ninja_forms_field_31_2" name="ninja_forms_field_31" type="radio" class="ninja-forms-field " value="Not sure" rel="31">Not sure <br /><br /><br />					
												
						<input class = "submit" align = "center" type="submit" name="formSubmit" value="Submit" />
						</p><br /><br /><br />
					</form>
                    </div>
					<div class="line">
						 <svg version="1.2" width="100%" height="100%">
						  <line x1="0" y1="5.5" x2="829" y2="5.5" stroke="#615e5e" stroke-width="2" stroke-dasharray="" stroke-linecap="square"></line>
						 </svg>
					</div>
					<br /><br />
                                        
                                        <div id="partner_1">
					<h1>Partner's / Support Person's Worksheet - <br />Active Surveillance </h1>
					
					<p style = "line-height: 140%; font-size: 15px;"><font color="#615e5e">Some pros and cons are listed in the table below, with a space at the bottom where you can write your thoughts if you wish.</font></p>
					<p></p>
					
					<form name="partner_form" action="" method="">
						<div id="partner_worksheet" class="table form" style="display:table; border-top:0px solid black;">	
							<div class="table-row">
								<div class="table-head" style="width: 10%">&nbsp;</div>
								<div class="table-head" style="width: 40%">Points to consider</div>
								<div class="table-head" style="width: 25%">Personal response</div>
								<div class="table-head" style="width: 25%">Level of importance</div>
							</div>
							<div class="table-row">
								<div class="table-cell"> 
								</div>
								<div class="table-cell">
										There are no side effects or complications from surgery or radiotherapy
								</div>
								<div class="table-cell">
								</div>
								<div class="table-cell">	 
									<input type="checkbox" name="very" value="A" /> Very important<br />
									<input type="checkbox" name="somewhat" value="B" /> Somewhat important<br />
									<input type="checkbox" name="not" value="C" /> Not important<br /> 
								</div>									
							</div>
							
							<div class="table-row">
								<div class="table-cell"> 
								</div>
								<div class="table-cell">
										The man may delay treatment side effects or never need other treatment
								</div>
								<div class="table-cell">
								</div>
								<div class="table-cell">	 
									<input type="checkbox" name="very" value="A" /> Very important<br />
									<input type="checkbox" name="somewhat" value="B" /> Somewhat important<br />
									<input type="checkbox" name="not" value="C" /> Not important<br /> 
								</div>								
							</div>
							
							
							<div class="table-row">
								<div class="table-cell"> 
								</div>
								<div class="table-cell">
										The man is closely monitored
								</div>
								<div class="table-cell">
								</div>
								<div class="table-cell">	 
									<input type="checkbox" name="very" value="A" /> Very important<br />
									<input type="checkbox" name="somewhat" value="B" /> Somewhat important<br />
									<input type="checkbox" name="not" value="C" /> Not important<br /> 
								</div>								
							</div>
							
							
							<div class="table-row">
								<div class="table-cell"> 
								</div>
								<div class="table-cell">
										If you are a partner, you can both live relatively normally
								</div>
								<div class="table-cell">
								</div>
								<div class="table-cell">	 
									<input type="checkbox" name="very" value="A" /> Very important<br />
									<input type="checkbox" name="somewhat" value="B" /> Somewhat important<br />
									<input type="checkbox" name="not" value="C" /> Not important<br /> 
								</div>								
							</div>
							
							
							<div class="table-row">
								<div class="table-cell"> 
								</div>
								<div class="table-cell">
										Other pros:
								</div>
								<div class="table-cell">
								</div>
								<div class="table-cell">	 
									<input type="checkbox" name="very" value="A" /> Very important<br />
									<input type="checkbox" name="somewhat" value="B" /> Somewhat important<br />
									<input type="checkbox" name="not" value="C" /> Not important<br /> 
								</div>								
							</div>
							<div class="line">
								 <svg version="1.2" width="100%" height="100%">
								  <line x1="0" y1="5.5" x2="829" y2="5.5" stroke="#615e5e" stroke-width="2" stroke-dasharray="" stroke-linecap="square"></line>
								 </svg>
							</div>
						
							<div class="table-row">
								<div class="table-cell"> 
								</div>
								<div class="table-cell">
										In the rare event of the prostate cancer spreading it may not be curable
								</div>
								<div class="table-cell">
								</div>
								<div class="table-cell">	 
									<input type="checkbox" name="very" value="A" /> Very important<br />
									<input type="checkbox" name="somewhat" value="B" /> Somewhat important<br />
									<input type="checkbox" name="not" value="C" /> Not important<br /> 
								</div>									
							</div>
							
							<div class="table-row">
								<div class="table-cell"> 
								</div>
								<div class="table-cell">
										The prostate cancer may grow and cause problems
								</div>
								<div class="table-cell">
								</div>
								<div class="table-cell">	 
									<input type="checkbox" name="very" value="A" /> Very important<br />
									<input type="checkbox" name="somewhat" value="B" /> Somewhat important<br />
									<input type="checkbox" name="not" value="C" /> Not important<br /> 
								</div>								
							</div>
							
							<div class="table-row">
								<div class="table-cell"> 
								</div>
								<div class="table-cell">
										Side effects from biopsies and inconvenience with regular testing can be distressing for me as a partner/ support person
								</div>
								<div class="table-cell">
								</div>
								<div class="table-cell">	 
									<input type="checkbox" name="very" value="A" /> Very important<br />
									<input type="checkbox" name="somewhat" value="B" /> Somewhat important<br />
									<input type="checkbox" name="not" value="C" /> Not important<br /> 
								</div>									
							</div>
							
							<div class="table-row">
								<div class="table-cell"> 
								</div>
								<div class="table-cell">
										It can be hard to live with uncertainty
								</div>
								<div class="table-cell">
								</div>
								<div class="table-cell">	 
									<input type="checkbox" name="very" value="A" /> Very important<br />
									<input type="checkbox" name="somewhat" value="B" /> Somewhat important<br />
									<input type="checkbox" name="not" value="C" /> Not important<br /> 
								</div>								
							</div>
							
							<div class="table-row">
								<div class="table-cell"> 
								</div>
								<div class="table-cell">
										Other cons:
								</div>
								<div class="table-cell">
								</div>
								<div class="table-cell">	 
									<input type="checkbox" name="very" value="A" /> Very important<br />
									<input type="checkbox" name="somewhat" value="B" /> Somewhat important<br />
									<input type="checkbox" name="not" value="C" /> Not important<br /> 
								</div>								
							</div>							
						</div><br /><br /> <br />
						<p class="para">
							<strong>Overall, are the pros or cons more important to me?</strong> &nbsp;&nbsp;
							<input type="checkbox" name="very" value="A" />Pros &nbsp;&nbsp;
							<input type="checkbox" name="somewhat" value="B" />Cons &nbsp;&nbsp;
							<input type="checkbox" name="not" value="C" />Not sure <br /><br /><br />					
												
						<input class="submit" align = "center" type="submit" name="formSubmit" value="Submit" />
                                                
						</p><br /><br /><br /><br />
					</form>
                                        </div>
				</div>
					
			</div>
        </div>
     </div>
</div>
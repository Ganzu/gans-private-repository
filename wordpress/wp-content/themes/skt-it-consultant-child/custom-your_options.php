 <?php
	/*
	Template Name: Your Options Custom Page
	*/
	get_header(); 
?>

	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'custom-your_options-page', 'custom-your_options' ); ?>
	<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
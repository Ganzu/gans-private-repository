<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package SKT IT Consultant
 */
?>
<div class="clear"></div>
         
</div><!--end .main-wrapper-->
<!--****************Customize Footer by Dahhrie-->

<div id="partner-logo" class="partner">
	<div class="partner-inner">
    	<img class="partner-img" src=<?php bloginfo('stylesheet_directory');?>/images/logo-1.png />
		<img class="partner-img" src=<?php bloginfo('stylesheet_directory');?>/images/logo-2.jpg />
		<img class="partner-img" src=<?php bloginfo('stylesheet_directory');?>/images/logo-3.png />
		<img class="partner-img" src=<?php bloginfo('stylesheet_directory');?>/images/logo-4.png />
		<img class="partner-img" src=<?php bloginfo('stylesheet_directory');?>/images/logo-5.png />
		<img class="partner-img" src=<?php bloginfo('stylesheet_directory');?>/images/logo-6.png />    
	   <div class="clear"></div>
    </div>
</div>
<footer id="footer">
	
	<div class="container">		
        	 <aside class="widget first">
             <!--   <h3 class="widget-title"><?php echo get_theme_mod('link_title', 'QUICK LINKS'); ?></h3> -->
                <div class="social-icons">
                 <a target="_blank" href="<?php echo esc_url(get_theme_mod('about_link', '#about')); ?>" title="About" ><span><?php _e('ABOUT | ','itconsultant'); ?></span></a>
                 <a target="_blank" href="<?php echo esc_url(get_theme_mod('resource_link', '#information')); ?>" title="Information" ><span><?php _e('INFORMATION | ','itconsultant'); ?></span></a>
                 <a target="_blank" href="<?php echo esc_url(get_theme_mod('work_link', '#worksheets')); ?>" title="Worksheets" ><span><?php _e('WORKSHEETS | ','itconsultant'); ?></span></a>
                 <a target="_blank" href="<?php echo esc_url(get_theme_mod('video_link', '#videos')); ?>" title="Videos" ><span><?php _e('VIDEOS | ','itconsultant'); ?></span></a>
                 <a target="_blank" href="<?php echo esc_url(get_theme_mod('contact_link', '#support')); ?>" title="support" ><span><?php _e('SUPPORT & RESOURCES | ','itconsultant'); ?></span></a>
				 <a target="_blank" href="<?php echo esc_url(get_theme_mod('privacy_link', '#privacy')); ?>" title="privacy" ><span><?php _e('PRIVACY','itconsultant'); ?></span></a>
				</div>
	        </aside>
			<!-- 
		<?php if ( ! dynamic_sidebar( 'footer-tweet' ) ) : ?>
	        <aside class="widget second">
                <h3 class="widget-title"><?php echo get_theme_mod('term_title', 'TERMS / LEGAL'); ?></h3> 
              	<div class="social-icons">
                 <a target="_blank" href="<?php echo esc_url(get_theme_mod('terms_link', '#terms')); ?>" title="Terms" ><span><?php _e('TERMS & CONDITIONS','itconsultant'); ?></span></a>
                 <a target="_blank" href="<?php echo esc_url(get_theme_mod('privacy_link', '#privacy')); ?>" title="Privacy" ><span><?php _e('PRIVACY','itconsultant'); ?></span></a>
               </div>
	        </aside>
		<?php endif; // end sidebar widget area ?> -->
		
        <div class="clear"></div>
    </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
<?php
/**
 * @package SKT IT Consultant
 * This page is a template for the Sub Resource & Forums Page
 */
 //echo $_GET['page_id'];exit;

?>
<script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
<script>
$(function(){
  $('a').each(function() {
    if ($(this).prop('href') == window.location.href) {
      $(this).addClass('current');
    }
  });
});
</script> 
 <?php 
	$pages = get_pages('parent=73');
	//print_r($pages);exit;
 ?>
 
<div class="content-area">
	<div class="customContainer">
		<div id="secondNav1">
			<img src="<?php bloginfo('stylesheet_directory');?>/images/video_icon.png" width="20px" height="20px"/>
			VIDEO GALLERY
		</div>
		<!--Outer Container-->
		<div id="infoContainer1">
			<!-- Left side navigation of info items -->
            <div id="leftInfoNavigation1">
				<!-- Entire set for one item in the info nav -->
				<div class="leftInfoHeading1">
					<h2><?php echo get_cat_name(13); ?></h2>				
					<div class="expandableButton1"></div>			
				</div>
				<div class="leftInfoContent1">
					<?php query_posts('cat=13'); ?>
                    <?php while (have_posts()) : the_post(); ?>
					<?php foreach ($pages as $page){?>
					<?php if($page->post_title == get_the_title()){?>
					<p class="content_<?php the_ID();?> content_title_navi1 articleTitleLeft1"  ><a href="<?php echo get_page_link($page->ID)?>"><?php the_title(); ?></a></p>
					<?php }?>
					<?php }?>
				   <?php endwhile; wp_reset_query(); ?>
				</div>
				
				
				<!-- Info nav set ends here -->
			</div>
			
				<!--Right side Container-->
			<div class="rightContentInfo1">
				<div class="rightArticleContainer1">	
				
			<!---------Chat Rooms Page(p83)-------------->
			<?php if(is_page('83')){?>
			
		
                	<?php query_posts('p=57')?>
					<?php while (have_posts()) : the_post(); ?>	
					<div>
                   		<!--you can add new class names but don't change current class names -->
                   		<div class="content-title1 content_<?php the_ID();?> rightArticleTitle1">
                   			<?php the_title();?>
                   		</div>
						<div> <h4>Prostate cancer support groups offer support and information to men with cancer and, often, to their family and carers. </h4></div>
                   		<div class="content-info1 "><?php the_content(); ?></div>
                	</div>
                	<?php  endwhile;wp_reset_query(); ?>
			<?php }?>
			  </div>	
            </div>
			<!--Right side Container-->
			<div class="rightContentInfo1">
				<div class="rightArticleContainer1">	
			<!------------------ends page display--------------->
		<!---------Health & Lifestyle Page(p86)-------------->
			<?php if(is_page('86')){?>
			
                	<?php query_posts('p=55')?>
					<?php while (have_posts()) : the_post(); ?>	
					<div>
                   		<!--you can add new class names but don't change current class names -->
                   		<div class="content-title1 content_<?php the_ID();?> rightArticleTitle1" >
                   			<?php the_title();?>
                   		</div>
					<div> <h4>Prostate cancer support groups offer support and information to men with cancer and, often, to their family and carers. </h4></div>
                   		<div class="content-info1 "><?php the_content(); ?></div>
                	</div>
                	<?php  endwhile;wp_reset_query(); ?>
			<?php }?>
			  </div>	
            </div>

        <!------------------ends page display--------------->
		<!--Right side Container-->
			<div class="rightContentInfo1">
				<div class="rightArticleContainer1">	
		<!---------Support Groups Page(p88)-------------->
			<?php if(is_page('88')){?>
			
                	<?php query_posts('p=53')?>
					<?php while (have_posts()) : the_post(); ?>	
					<div>
                   		<!--you can add new class names but don't change current class names -->
                   		<div class="content-title1 content_<?php the_ID();?> rightArticleTitle1" >
                   			<?php the_title();?>
                   		</div>
						<div> <h4>Prostate cancer support groups offer support and information to men with cancer and, often, to their family and carers. </h4></div>
                   		<div class="content-info1 "><?php the_content(); ?></div>
                	</div>
                	<?php  endwhile;wp_reset_query(); ?>
			<?php }?>
			  </div>	
            </div>

        <!------------------ends page display--------------->
			<!--Right side Container-->
			<div class="rightContentInfo1">
				<div class="rightArticleContainer1">	
			<!---------Complementary Therapies Page(p93)-------------->
			<?php if(is_page('93')){?>
			
                	<?php query_posts('p=51')?>
					<?php while (have_posts()) : the_post(); ?>	
					<div>
                   		<!--you can add new class names but don't change current class names -->
                   		<div class="content-title1 content_<?php the_ID();?> rightArticleTitle1" >
                   			<?php the_title();?>
                   		</div>
						<div> <h4>Prostate cancer support groups offer support and information to men with cancer and, often, to their family and carers. </h4></div>
                   		<div class="content-info1 "><?php the_content(); ?></div>
                	</div>
                	<?php  endwhile;wp_reset_query(); ?>
			<?php }?>
			  </div>	
            </div>

        <!------------------ends page display--------------->
			<!--Right side Container-->
			<div class="rightContentInfo1">
				<div class="rightArticleContainer1">	
			<!---------Support Groups Page(p97)-------------->
			<?php if(is_page('97')){?>
			
                	<?php query_posts('p=48')?>
					<?php while (have_posts()) : the_post(); ?>	
					<div>
                   		<!--you can add new class names but don't change current class names -->
                   		<div class="content-title1 content_<?php the_ID();?> rightArticleTitle1" >
                   			<?php the_title();?>
                   		</div>
						<div> <h4>Prostate cancer support groups offer support and information to men with cancer and, often, to their family and carers. </h4></div>
                   		<div class="content-info1 "><?php the_content(); ?></div>
                	</div>
                	<?php  endwhile;wp_reset_query(); ?>
			<?php }?>
			  </div>	
            </div>

        <!------------------ends page display--------------->
		  </div>
    </div>
</div>
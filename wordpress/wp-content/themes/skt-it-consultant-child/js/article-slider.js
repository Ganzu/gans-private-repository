/*
* This js file uses the jquery.diyslider library
*/

/* Articles for you slider */
jQuery(document).ready(function($) {
	//Resize just happened, pixels changed
	$(window).resize(function() {
		var newWidth = $("#test").width();
		var widthAsString = newWidth.toString() + "px";
  		$(".sa-slider").diyslider("resize", widthAsString, "480px");
	});
	//Initialize slider
	$(".sa-slider").diyslider({
		width: $("#test").width().toString() + "px",
		height: "480px",
    	display: 1, // number of slides you want it to display at once
    	loop: false // disable looping on slides
	});
	// use buttons to change slide
	$("#slider-left-button").bind("click", function(){
    	$(".sa-slider").diyslider("move", "back");
	});
	$("#slider-right-button").bind("click", function(){
    	$(".sa-slider").diyslider("move", "forth");
	});
});

/*
* This file contains additional jQuery functions
* 1. toolTip functionality
* 2. stickyWidget functionality
*/

jQuery(document).ready(function($) {

	/**************** Input Placeholder Text ****************/
	$(".username").attr("placeholder", "Username");
	$(".password").attr("placeholder", "Password");
	//On focus
	$(".user-login").focus(function() {
  		$(this).attr("placeholder", "");
	});
	//Out of focus
	$(".username").focusout(function() {
		$(".username").attr("placeholder", "Username");
	});
	$(".password").focusout(function() {
		$(".password").attr("placeholder", "Password");
	});

	/**************** Initialization CSS ****************/
	$("#password-recovery").css({"line-height": $("#password-recovery").height() + "px"});

	/**************** Window Resize CSS ****************/
	$(window).resize(function() {
		$("#password-recovery").css({"line-height": $("#password-recovery").height() + "px"});
	});

	/**************** Sticky Widget ****************/
	if (!!$('.sticky').offset()) { // make sure ".sticky" element exists 	
	    var stickyTop = $('.sticky').offset().top; // returns number 
	    $(window).scroll(function(){ // scroll event
	    	var windowTop = $(window).scrollTop(); // returns number  		
	 		$('.sticky').clearQueue();		

	 		setInterval(function(){
	 		},1000);

	    	if (stickyTop < windowTop){      	      	      	        
	      		$('.sticky').css({"position": "fixed"});
	      		$('.sticky').show();        
	        	$(".sticky").animate({"opacity":"1", "top": "40%"}, 800);            	
	      	}
	      	else {
	      		$('.sticky').css({"position": "absolute", "top": "10px", "opacity": "0"});
	      		$('.sticky').hide();
	      		$('.sticky').stop();
	        	$('.sticky').clearQueue();
	      	}
	    });	 
  	}

  	/**************** Sticky Widget Scroll Top ****************/
  	$(".sticky").click(function(){
  		$("html, body").animate({ scrollTop: 0 }, "slow");
  	});

  	/**************** Sticky Hover ****************/
  	$(".sticky").click(function(){
  		$("html, body").animate({ scrollTop: 0 }, "slow");
  	});

	/**************** Tool Tip ****************/
	//Get initial position
	var initPosition = $(".toolTip").position();

	//Detects position of mouse cursor
	var mouseX;
	var mouseY;
	$(document).mousemove( function(e) {
   		mouseX = e.pageX + 10; 
   		mouseY = e.pageY - 30;
	});

	// Mouse over reveal tooltip box
	$(".moreInfo").mouseover(function() {
		$(".toolTip").show();
	    $(".toolTip").animate({"opacity":"1", "top":mouseY, "left":mouseX, "width":"auto"}, 300);
	}).mouseout(function() {
	    $(".toolTip").hide();
	    $(".toolTip").animate({"opacity":"0", "top":initPosition.top , "left":initPosition.left});
	});

	/**************** Test ****************/
	$("#testButton").click(function(){
  // 		$.ajax({
  //    		url: 'http://localhost:8888/test.php', //This is the current doc
  //    		type: "GET",
  //    		dataType: 'jsonp', // add json datatype to get json
  //       	success: function(data){   
  //    			console.log(data);
  //    		},
  //    		error: function(xhr, status, thrown){
  //   			alert("fail");
  //   			console.log(xhr, status, thrown);
  // 			}
		// });
	$.get("http://localhost:8888/wordpress/test.php",function(data){
		console.log(data);
		var parsed = JSON.parse(data);
		var array = new Array();
		for(var x in parsed){
			array.push(parsed[x]);
		}
		console.log(array);
	});
		
  	});

});
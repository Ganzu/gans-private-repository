/* Left Content Info */
jQuery(document).ready(function($) {
  //Initial setting when page first loads
  $(".leftInfoContent").hide();
  $(".leftInfoHeading:first").css({"background-color": "#062340", "border-top": "1px solid #B5B5B5"});//sets background colour of first occurence
  $(".leftInfoHeading:first .expandableButton").css({"border-color": "#FFF"});
  $(".leftInfoHeading:first h2").css({"color": "#FFF"});

  //Hide all content before hand
  $(".articleBlock").hide();
  $(".articleBlock:first").show();

  //On click function
  $(".leftInfoHeading").click(function(event){
    //Toggles active class
    $(this).toggleClass('.active');
    $(".leftInfoHeading").not($(this)).css({"background-color": "transparent"});//change background colour of other heading element
    $(".leftInfoHeading h2").not($(this)).css({"color": "#062340"});//change colour of other h2 element    
    $('.leftInfoContent').not($(this).next(".leftInfoContent")).each(function(){//selects all other .leftInfoContent but itself
         $(this).slideUp();
         $(".expandableButton.left").css({"border-right": "3px solid #062340", "border-bottom": "3px solid #062340", "border-left": "none", "border-top": "none"});   ;
    });
    $(this).next(".leftInfoContent").slideToggle(400);
    //If active/ not active
    if($(this).hasClass(".active")){
      $(this).children(".expandableButton").css({"border-left": "3px solid #FFF", "border-top": "3px solid #FFF", "border-right": "none", "border-bottom": "none"});
    }
    else{
      $(this).children(".expandableButton").css({"border-right": "3px solid #FFF", "border-bottom": "3px solid #FFF", "border-left": "none", "border-top": "none"}); 
    }
    //Fixed css for selected element
    $(this).children(".leftInfoHeading h2").css({"color": "#FFF"});
    $(this).css({"background-color": "#062340"});//Change background color of selected element

    //Hides/show appropriate article blocks
    var classname = $(this).attr('class');
    var classArr = classname.split(" ");
    var target = $(".rightContentInfo ."+ classArr[0]);
    console.log(classArr);
    $(".articleBlock").hide();
    target.show();

  });

});

/* Right content info */
jQuery(function($){
  //Hide all content before hand
  $(".content-info").hide();  

  //Right content on click function
  $(".content-title").click(function(){
    //Toggles active
    $(this).toggleClass('.active');
    $(this).next().slideToggle(400);
    if($(this).hasClass(".active")){
      $(this).children(".expandableButton").css({"border-left": "3px solid #062340", "border-top": "3px solid #062340", "border-right": "none", "border-bottom": "none"});
    } 
    else{
      $(this).children(".expandableButton").css({"border-right": "3px solid #062340", "border-bottom": "3px solid #062340", "border-left": "none", "border-top": "none"}); 
    }

  });
});

/* Click left navi title */
jQuery(function($){
  $(".content_title_navi").click(function(){
    var classname = $(this).attr('class');
    var classArr = classname.split(" ");
    var target = $(".rightContentInfo ."+ classArr[0]).next();    
    target.slideToggle(400);
    console.log(classArr);
    //Toggles active & change styling of arrow
    var subTarget = target.prev();
    subTarget.toggleClass('.active');
    if(subTarget.hasClass(".active")){
      subTarget.children(".expandableButton").css({"border-left": "3px solid #062340", "border-top": "3px solid #062340", "border-right": "none", "border-bottom": "none"});
    }
    else{
      subTarget.children(".expandableButton").css({"border-right": "3px solid #062340", "border-bottom": "3px solid #062340", "border-left": "none", "border-top": "none"}); 
    }

    //Scroll to the selected article
    if(target.length) {
      event.preventDefault();
      $('html, body').animate({
        scrollTop: target.offset().top - 100
      }, 1000);
    }
  });
});

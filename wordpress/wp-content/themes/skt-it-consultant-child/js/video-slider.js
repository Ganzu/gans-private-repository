/*
* This js file uses the jquery.diyslider library
*/

/* Slider Functions */
jQuery(document).ready(function($) {
	//Resize just happened, pixels changed
	$(window).resize(function() {

		if ($('.sa-slider').length){
        	var newWidth2 = $("#test").width();
			var widthAsString2 = newWidth2.toString() + "px";
  			$(".sa-slider").diyslider("resize", widthAsString2, "500px");
    	}
		else if($('.sv-slider').length){
		var newWidth = $("#videoSliderContainer").width();
		var widthAsString = newWidth.toString() + "px";
		$(".sv-slider").diyslider("resize", widthAsString, "450px");
		}		
	});

	$(".sv-slider").each(function(){
    	$(this).diyslider({
	    width: $("#videoSliderContainer").width().toString() + "px",
	    height: "450px", // height of the slider
	    display: 1, // number of slides you want it to display at once
	    loop: false // disable looping on slides
		});
	}); // this is all you need!

	// use buttons to change slide
	$("#video-slider-left-button").bind("click", function(){
    	$(".sv-slider").diyslider("move", "back");
	});
	$("#video-slider-right-button").bind("click", function(){
    	$(".sv-slider").diyslider("move", "forth");
	});

	//Initialize slider
	$(".sa-slider").diyslider({
		width: $("#test").width().toString() + "px",
		height: "500px",
    	display: 1, // number of slides you want it to display at once
    	loop: false // disable looping on slides
	});
	// use buttons to change slide
	$("#slider-left-button").bind("click", function(){
    	$(".sa-slider").diyslider("move", "back");
	});
	$("#slider-right-button").bind("click", function(){
    	$(".sa-slider").diyslider("move", "forth");
	});

});
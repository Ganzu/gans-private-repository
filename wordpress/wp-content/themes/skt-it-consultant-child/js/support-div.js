/*
  Left Content Info
*/
jQuery(document).ready(function($) {
  //Initial setting when page first loads
 // $(".leftInfoContent1").hide();
  //$(".content-title1").hide();
  //$(".content-info1").hide();
  $(".leftInfoHeading1:first").css({"background-color": "#062340", "border-top": "1px solid #B5B5B5"});//sets background colour of first occurence
  $(".leftInfoHeading1:first .expandableButton1").css({"border-color": "#FFF"});
  $(".leftInfoHeading1:first h2").css({"color": "#FFF"});
  $(".expandableButton1").css({"border-right": "3px solid #FFF", "border-bottom": "3px solid #FFF", "border-left": "none", "border-top": "none"});   ;
  //On click function
  $(".leftInfoHeading1").click(function()
  {
    $(this).siblings('.leftInfoHeading1').removeClass('.active');
    $(this).toggleClass('.active');

    $(".leftInfoHeading1").not($(this)).css({"background-color": "#062340"});//change background colour of other heading element
    $(".leftInfoHeading1 h2").not($(this)).css({"color": "#062340"});//change colour of other h2 element
    
    $('.leftInfoContent1').not($(this).next(".leftInfoContent1")).each(function(){//selects all other .leftInfoContent but itself
         $(this).slideUp();
         $(".expandableButton1").css({"border-right": "3px solid #062340", "border-bottom": "3px solid #062340", "border-left": "none", "border-top": "none"});   ;
    });

    $(this).next(".leftInfoContent1").slideToggle(400);

    //If active/ not active
    if($(this).hasClass(".active")){
      $(this).children(".expandableButton1").css({"border-left": "3px solid #FFF", "border-top": "3px solid #FFF", "border-right": "none", "border-bottom": "none"});
	
	}
    else{
      $(this).children(".expandableButton1").css({"border-right": "3px solid #FFF", "border-bottom": "3px solid #FFF", "border-left": "none", "border-top": "none"}); 
    }

    //Fixed css for selected element
    $(this).children(".leftInfoHeading1 h2").css({"color": "#FFF"});
    $(this).css({"background-color": "#062340"});//Change background color of selected element


  });
});

/*
 * Right content info
 */
/**
 * 
 * click title
 */
jQuery(function($){
  //Hide all content before hand
  //$(".content-info1").hide();
  //Right content on click function
  $(".content-title1").click(function(){
    //Toggles active
    $(this).siblings('.content-title1').removeClass('.active');
    $(this).toggleClass('.active');
	
      $(this).next().slideToggle(400);
    if($(this).hasClass(".active")){		
	      $(this).children(".expandableButton1").css({"border-left": "none", "border-top": "none", "border-right": "none", "border-bottom": "none"});	
  }
    else{
      $(this).children(".expandableButton1").css({"border-right": "none", "border-bottom": "none", "border-left": "none", "border-top": "none"}); 
    }

  });
});
/**
 * 
 * click left navi title
 */
jQuery(function($){
  $(".content_title_navi1").click(function(){
    var classname = $(this).attr('class');
    var classArr = classname.split(" ");
	$(".rightArticleContainer1 ."+ classArr[0]).next().slideToggle(400);
	//$(".rightArticleContainer1 ."+ classArr[0]).parent().siblings().children().next().slideUp();
  });
});

 <?php
	/*
	Template Name: Support & Resources Page
	*/
	get_header(); 
?>

	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'custom-05-content', 'custom-05' ); ?>
	<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
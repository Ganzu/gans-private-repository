<?php
/**
 * @package SKT IT Consultant
 */
?>
<div class="content-area">
	<div class="customContainer" style = "background-color: #ffffff;">
		<div id="secondNav">
			<img src="<?php bloginfo('stylesheet_directory');?>/images/video_icon.png" width="20px" height="20px"/>
			VIDEO GALLERY
		</div>

		<!-- Now Playing Video -->
		<div id="videoPlayingContainer">
			<div id = "genericVideoDescription">
				<p style = "font-size: 32px">Video Gallery</p>			
				<p>You're not alone. We've interviewed and documented over 40 men of different ages with various diagnosis' of Prostate Cancer. By doing this, we hope to help you understand the options available to you.</p>				
				<span><p style = "font-size: 15px;"><font color="#615e5e">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.</font></p></span>
			</div>		
			<div id="genericVideoPlaying">
				<!-- Section where the retrived video gets displayed -->
				<div id="genericVideo">
					<iframe width="483" height="325" src="https://www.youtube.com/embed/oLQtT1d_QBo?rel=0" frameborder="0" allowfullscreen></iframe>								
				</div>
			</div>
		</div>	
		
		
		
		<div id="genericSuggestedVideoContainer">
			<!-- Title: Videos For You -->
			
						
					<?php $counter = 0;
						query_posts(array('category__and'=>array(12,3),'orderby'=>rand));
						if(have_posts()) : 
						?>
						<h1 id="videoCategory"><?php echo get_cat_name(3);?></h1>
						<div id = "videos"> 
							<div id= "video-row">
							<?php
							while(have_posts()) : the_post();
								if((($counter%4)==0) && ($counter!=0)) : 
								?>
								</div>
								<div id= "video-row">
								<?php
								endif;
								$originalContent = get_the_content();
								//echo $originalContent;
								$content = preg_split('/[\s]+/',get_the_content());
								//remove the tags from the start and end of the link
								$link = substr(substr($content[0],7),0,-8); 
								//$link = substr($content,0,-8);
								$description = str_replace($content[0],'',$originalContent);
					?>
					
				
						<div class="videosContainer">
							<iframe width="281px" height="220px" src="<?php echo $link?>" frameborder="0" allowfullscreen></iframe>
							<!-- Video title -->
							<div class="genericVideoTextContainerMini">
								<h1 class="videoTitleMini"><?php the_title('');?></h1>
								<?php echo $description; $counter++; ?> 
							</div>
							<!-- Video description -->
						</div>
							<?php
								endwhile;
								endif;
								wp_reset_query();
							?>
				</div>	
			</div>
					
			<?php $counter = 0;
				query_posts(array('category__and'=>array(12,8),'orderby'=>rand));
				if(have_posts()) : 
				?>
				<h1 id="videoCategory"><?php echo get_cat_name(8);?></h1>
				<div id = "videos"> 
					<div id= "video-row">
					<?php
						while(have_posts()) : the_post();
							if((($counter%4)==0) && ($counter!=0)) : 
							?>
							</div>
							<div id= "video-row">
							<?php
								endif;
							$originalContent = get_the_content();
							//echo $originalContent;
							$content = preg_split('/[\s]+/',get_the_content());
							//remove the tags from the start and end of the link
							$link = substr(substr($content[0],7),0,-8); 
							//$link = substr($content,0,-8);
							$description = str_replace($content[0],'',$originalContent);
							?>
			
		
							<div class="videosContainer">
								<iframe width="281px" height="220px" src="<?php echo $link?>" frameborder="0" allowfullscreen></iframe>
								<!-- Video title -->
								<div class="genericVideoTextContainerMini">
									<h1 class="videoTitleMini"><?php the_title('');?></h1>
									<?php echo $description; $counter++; ?> 
								</div>
								<!-- Video description -->
							</div>
							<?php
								endwhile;
								endif;
								wp_reset_query();
							?>
						</div>	
				</div>
						
								
				<?php $counter = 0;
					query_posts(array('category__and'=>array(12,6),'orderby'=>rand));
					if(have_posts()) : 
					?>
					<h1 id="videoCategory"><?php echo get_cat_name(6);?></h1>
					<div id = "videos"> 
						<div id= "video-row">
						<?php
						while(have_posts()) : the_post();
							if((($counter%4)==0) && ($counter!=0)) : 
							?>
								</div>
								<div id= "video-row">
							<?php
							endif;
						$originalContent = get_the_content();
						//echo $originalContent;
						$content = preg_split('/[\s]+/',get_the_content());
						//remove the tags from the start and end of the link
						$link = substr(substr($content[0],7),0,-8); 
						//$link = substr($content,0,-8);
						$description = str_replace($content[0],'',$originalContent);
						?>		
						<div class="videosContainer">
							<iframe width="281px" height="220px" src="<?php echo $link?>" frameborder="0" allowfullscreen></iframe>
							<!-- Video title -->
							<div class="genericVideoTextContainerMini">
								<h1 class="videoTitleMini"><?php the_title('');?></h1>
								<?php echo $description; $counter++; ?> 
							</div>
							<!-- Video description -->
						</div>
						<?php
							endwhile;
							endif;
							wp_reset_query();
						?>
					</div>	
				</div>			
	    </div>
	</div>
</div>
<?php
/**
 * @package SKT IT Consultant
 * This page is a template for the Information page
 */
?>
<div class="content-area">
	<div class="customContainer">
		<?php require_once("secondNav.php");?>
		<!--Outer Container-->
		<div id="infoContainer">
			<!-- Left side navigation of info items -->
            <div id="leftInfoNavigation">
				<!-- Entire set for one item in the info nav -->
				<?php 
					$articleCount=1;
					$subArticleCount=1;
					$categories = get_categories('child_of=2'); 
					//Iterate through each subcategory
					foreach ($categories as $category) {
  						$option = '<option value="/category/archives/'.$category->category_nicename.'">';
						$option .= $category->cat_name;						
						$option .= '</option>';
						echo $option;
					}
				?>
				<div class="leftInfoHeading">					
					<h2><?php echo $articleCount . ": " . $categories; ?></h2>		
					<div class="expandableButton"></div>			
				</div>

				<div class="leftInfoContent">
					<?php query_posts('cat=2');?>
                    <?php while (have_posts()) : the_post();?>
					<p class="content_<?php the_ID();?> content_title_navi articleTitleLeft"><?php echo $articleCount . "." . $subArticleCount . "&nbsp&nbsp;";?><?php the_title();?></p>
					<?php $subArticleCount++;?>
                    <?php endwhile; wp_reset_query(); ?>
				</div>
				<!-- Info nav set ends here -->
				<!-- Entire set for one item in the info nav -->
				<div class="leftInfoHeading">
					<h2>Sona</h2>		
					<div class="expandableButton"></div>			
				</div>
				<div class="leftInfoContent">
					<p>Lorem ipsum stuff</p>
					<p>More lorem ipsum stuff</p>
				</div>
				<!-- Info nav set ends here -->
				<!-- Entire set for one item in the info nav -->
				<div class="leftInfoHeading">
					<h2>Header-1</h2>		
					<div class="expandableButton"></div>			
				</div>
				<div class="leftInfoContent">
					<p>Lorem ipsum stuff</p>
					<p>More lorem ipsum stuff</p>
				</div>
				<!-- Info nav set ends here -->
			</div>
			<!--Right side Container-->
            <div class="rightContentInfo">
            	<div class="rightArticleContainer">
            		<?php
            			foreach ($categories as $category) {
  							$option = '<option value="/category/archives/'.$category->category_nicename.'">';
							$option .= $category->cat_name;						
							$option .= '</option>';
							echo $option;
						}
					?>
            		<!-- Display title -->
            		<h2><?php echo "Your Options: " . $articleCount . ". " . get_cat_name(2);?></h2>
                	<?php query_posts('cat=2'); ?>
                	<?php while (have_posts()) : the_post(); ?>
					<div>
                   		<!--you can add new class names but don't change current class names -->
                   		<div class="content-title content_<?php the_ID();?> rightArticleTitle">
                   			<?php the_title(); ?>
                   			<div class="expandableButton"></div>
                   		</div>
                   		<div class="content-info"><?php the_content();?></div>
                	</div>
                	<?php endwhile; wp_reset_query(); ?>
                </div>

            </div>
		</div>
	</div>
</div>
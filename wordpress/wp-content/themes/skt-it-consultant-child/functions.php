<?php
	function theme_enqueue_styles() {

	    $parent_style = 'parent-style';

	    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
	    wp_enqueue_style( 'child-style',
	        get_stylesheet_directory_uri() . '/style.css',
	        array( $parent_style )
	    );
	}
	add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

	//This function deques the parent javascript to read the childs script
	add_action('wp_enqueue_scripts', 'wpse26822_script_fix', 100);
	function wpse26822_script_fix(){
    	wp_dequeue_script('parent_theme_script_handle');
    	wp_enqueue_script('child_theme_script_handle', get_stylesheet_directory_uri().'/js/jquery.nivo.slider.js', array('jquery'));
	}

	//This function deques the parent nivo-slider css to read the childs nivo-slider css
	add_action( 'wp_enqueue_scripts', 'remove_default_stylesheet', 20 );
	function remove_default_stylesheet() {
    	wp_dequeue_style( 'original-enqueue-stylesheet-handle' );
    	wp_deregister_style( 'original-register-stylesheet-handle' );
    	wp_register_style( 'new-style', get_stylesheet_directory_uri() . '/css/nivo-slider.css', false, '1.0.0' ); 
    	wp_enqueue_style( 'new-style' );
	}	

	//Load jquery script Note** apparently get_stylesheet_directory_uri() refers to child theme
	//while get_template_directory_uri() is if it has no child
	function add_my_script() {
		wp_register_script('support-div',get_stylesheet_directory_uri() . '/js/support-div.js', array( 'jquery' ));
		wp_register_script('collapsable-div',get_stylesheet_directory_uri() . '/js/collapsable-div.js', array( 'jquery' ));
		wp_register_script('diy-slider',get_stylesheet_directory_uri() . '/js/jquery.diyslider.js', array( 'jquery' ));    	
    	wp_register_script('video-slider',get_stylesheet_directory_uri() . '/js/video-slider.js', array( 'jquery' ));    	
    	wp_register_script('extra-functions',get_stylesheet_directory_uri() . '/js/extra-functions.js', array( 'jquery' ));

    	wp_enqueue_script('extra-functions');
    	wp_enqueue_script('collapsable-div');
    	wp_enqueue_script('diy-slider');
    	wp_enqueue_script('video-slider');  
		wp_enqueue_script('support-div');		
	}  
	add_action( 'wp_enqueue_scripts', 'add_my_script' );


	//Function to retrieve first image of a post
	function catch_that_image() {
  		global $post, $posts;
  		$first_img = '';
  		ob_start();//php function turn on output buffering
  		ob_end_clean();
 		$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  		$first_img = $matches[1][0];

  		if(empty($first_img)) {
    		$first_img = "/path/to/default.png";
  		}
  		return $first_img;
	}

	//Function to remove image from posts in the_content() function
	//http://bavotasan.com/2011/a-better-way-to-remove-images-from-a-wordpress-post/
	function remove_images( $content ) {
   		$postOutput = preg_replace('/<img[^>]+./','', $content);
   		return $postOutput;
	}
	add_filter( 'the_content', 'remove_images', 100 );


	//Loading aqua resizer library
	require_once('aq_resizer.php');

	/*Remove <p> element when getting content -->
	<?php remove_filter ('the_content', 'wpautop');?>*/
?>
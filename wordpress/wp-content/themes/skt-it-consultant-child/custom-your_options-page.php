<?php
/**
 * @package SKT IT Consultant
 * This page is a template for the Your Options page
 */
?>
<div class="content-area">
	<div class="customContainer">
		<?php require_once("secondNav.php");?>
		<?php require_once("stickyWidget.php");?>
		<!--Outer Container-->
		<div id="infoContainer">
			<!-- Left side navigation of info items -->
            <div id="leftInfoNavigation">
				<!-- Entire set for one item in the info nav -->
				<?php 
					$articleCount=1;
					$subArticleCount=1;
					$categories = get_categories('child_of=2'); 
					//Iterate through each subcategory
					foreach ($categories as $category) : //Start foreach 
						//Get values of subcategory
  						//$option = '<option value="/category/archives/'.$category->category_nicename.'">';
						//$option .= $category->cat_name;						
						//$option .= '</option>';
						$catName = $category->cat_name;	
						$subCategory_ID = $category->cat_ID;
				?>
				<!-- Display leftInfoHeading first-->
				<div class="title_<?php echo $subCategory_ID;?> leftInfoHeading">					
					<h2><?php echo $articleCount . ": " . $catName; ?></h2>		
					<div class="expandableButton left"></div>			
				</div>

				<div class="leftInfoContent">
					<?php query_posts('cat='.$subCategory_ID);?>
                    <?php while (have_posts()) : the_post();?>
					<p class="content_<?php the_ID();?> content_title_navi articleTitleLeft"><?php echo $articleCount . "." . $subArticleCount . "&nbsp&nbsp;";?><?php the_title();?></p>
					<?php $subArticleCount++; ?>
                    <?php endwhile; wp_reset_query(); $subArticleCount=1;?>
                    <?php $articleCount++; ?>

				</div>
				<?php endforeach;//End foreach?>
				<div class="article-flow-button-container">
					<button class="btn-01 article-flow-button">View worksheets</button>
				</div>
			</div>

			<!--Right side Container-->
            <div class="rightContentInfo">
            	<div class="rightArticleContainer">            		
            		<!-- Entire set for one item in the info nav -->
					<?php 
						$articleCount=1;
						$subArticleCount=1;
						$categories = get_categories('child_of=2'); 
						//Iterate through each subcategory
						foreach ($categories as $category) : //Start foreach 
							//Get values of subcategory
  							//$option = '<option value="/category/archives/'.$category->category_nicename.'">';
							//$option .= $category->cat_name;						
							//$option .= '</option>';
							$catName = $category->cat_name;	
							$subCategory_ID = $category->cat_ID;
					?>
					<div class="title_<?php echo $subCategory_ID;?> articleBlock">
            			<!-- Display title -->
            			<h2 class="right-title"><?php echo "Treatment Options: " . $articleCount . ". " . $catName;?></h2>
                		<?php query_posts('cat=' . $subCategory_ID); ?>
                		<?php while (have_posts()) : the_post(); ?>					
                   		<!--you can add new class names but don't change current class names -->
                   		<div class="content-title content_<?php the_ID();?> rightArticleTitle">
                   			<?php the_title(); ?>
                   			<div class="expandableButton"></div>
                   		</div>
                   		<div class="content-info"><?php the_content();?></div>                	
                		<?php endwhile; wp_reset_query(); ?>
                	</div>
                	<?php $articleCount++; ?>
                	<?php endforeach;//End foreach?>

                </div>
            </div>

		</div>
	</div>
</div>
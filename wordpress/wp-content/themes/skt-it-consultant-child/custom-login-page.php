<?php
/**
 * @package SKT IT Consultant
 * This page is a template for the login page
 */
?>
<div class="content-area">
	<div class="customContainer">
		<div id="loginPartContainer">
			<div id="login-welcome-panel"><!-- Table -->
				<div id="login-container"><!-- Table Row -->

					<!-- Left side panel-->
					<div id="login-panel-left">
						<div class="login-title">
							<div id="logo">
		                 		<a href="<?php echo esc_url(home_url('/'));?>">
		                        	<h2><?php bloginfo( 'name' ); ?></h2>
		                        	<span><?php bloginfo( 'description' ); ?></span>
		                    	</a>
		                    </div>
		                </div>
		                <div id="welcome-image">
		                	<?php //Loads image based on the featured image set from the admin panel
								if ( has_post_thumbnail() ) {
									$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'full');
									$image = aq_resize( $url, 9999, 350, false );
									echo '<img src="'. $image. '" />';
								}
							?>
		                </div>
		            </div>

		            <!-- Right side panel-->
	            	<div id="login-panel-right">
	            		<div id="login-input-panel">
	            			<!-- Title -->
	            			<div class="login-title-right">
	            				<h2 class="right-title">Login</h2>	            			
	            			</div>	    
	            			<!-- Input Fields -->        		
	            			<input type="text" class="user-login username"/>
		            		<input type="password" class="user-login password"/>
		            		<!-- Password Recovery Link-->
		            		<div id="password-recovery">
		            			<a href="#">Forgot password ?</a>
		            		</div>
		            		<!-- Login Button -->
		            		<div id="login-button" action="">
		            			<button class="btn-01">Login</button>
		            		</div>
		            	</div>	
	            	</div>

	            	<!-- Test
	            	<button class="btn-01" id="testButton">testIt</button>
					-->

	            </div>
			</div>
		</div>
	</div>
</div>
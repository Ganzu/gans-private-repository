 <?php
	/*
	Template Name: Information Custom Page
	*/
	get_header(); 
?>

	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'custom-03-content', 'custom-03' ); ?>
	<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>